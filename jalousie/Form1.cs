﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jalousie
{
    public partial class formJalousie : Form
    {
        public formJalousie()
        {
            InitializeComponent();
            btnOK.Enabled = false;
        }

        private void TxtbxWidth_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void TxtbxWidth_TextChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = !string.IsNullOrEmpty(txtbxWidth.Text) && !string.IsNullOrEmpty(txtbxHeight.Text);
            lblOutput.Text = "";
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            double width = Convert.ToDouble(txtbxWidth.Text);
            double height = Convert.ToDouble(txtbxHeight.Text);
            double price = 0;
            double total;

            switch(cmbxMaterial.Text)
            {
                case "Пластик": price = 1080; break;
                case "Алюминий": price = 2580; break;
                case "Соломка": price = 4990; break;
                case "Текстиль": price = 660; break;
            }

            total = (width * height) / 10000 * price;

            lblOutput.Text = "Размер: "+width+"x"+height+" см.кв.\n";
            lblOutput.Text += "Цена: "+price.ToString("c")+" за м.кв.\n";
            lblOutput.Text += "Итого: "+total.ToString("c");
            
        }

        private void CmbxMaterial_TextChanged(object sender, EventArgs e)
        {
            lblOutput.Text = "";
        }
    }
}
