﻿namespace jalousie
{
    partial class formJalousie
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtbxWidth = new System.Windows.Forms.TextBox();
            this.txtbxHeight = new System.Windows.Forms.TextBox();
            this.cmbxMaterial = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblWidth
            // 
            this.lblWidth.AutoSize = true;
            this.lblWidth.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWidth.Location = new System.Drawing.Point(30, 24);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(111, 19);
            this.lblWidth.TabIndex = 0;
            this.lblWidth.Text = "Ширина (см.)";
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(30, 65);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(107, 19);
            this.lblHeight.TabIndex = 1;
            this.lblHeight.Text = "Высота (см.)";
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaterial.Location = new System.Drawing.Point(30, 111);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(90, 19);
            this.lblMaterial.TabIndex = 2;
            this.lblMaterial.Text = "Материал:";
            // 
            // txtbxWidth
            // 
            this.txtbxWidth.Location = new System.Drawing.Point(150, 23);
            this.txtbxWidth.Name = "txtbxWidth";
            this.txtbxWidth.Size = new System.Drawing.Size(138, 20);
            this.txtbxWidth.TabIndex = 3;
            this.txtbxWidth.TextChanged += new System.EventHandler(this.TxtbxWidth_TextChanged);
            this.txtbxWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtbxWidth_KeyPress);
            // 
            // txtbxHeight
            // 
            this.txtbxHeight.Location = new System.Drawing.Point(150, 64);
            this.txtbxHeight.Name = "txtbxHeight";
            this.txtbxHeight.Size = new System.Drawing.Size(138, 20);
            this.txtbxHeight.TabIndex = 4;
            this.txtbxHeight.TextChanged += new System.EventHandler(this.TxtbxWidth_TextChanged);
            this.txtbxHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtbxWidth_KeyPress);
            // 
            // cmbxMaterial
            // 
            this.cmbxMaterial.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbxMaterial.FormattingEnabled = true;
            this.cmbxMaterial.Items.AddRange(new object[] {
            "Пластик",
            "Алюминий",
            "Соломка",
            "Текстиль"});
            this.cmbxMaterial.Location = new System.Drawing.Point(150, 106);
            this.cmbxMaterial.Name = "cmbxMaterial";
            this.cmbxMaterial.Size = new System.Drawing.Size(138, 24);
            this.cmbxMaterial.TabIndex = 5;
            this.cmbxMaterial.TextChanged += new System.EventHandler(this.CmbxMaterial_TextChanged);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(34, 148);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(97, 27);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.BackColor = System.Drawing.SystemColors.Control;
            this.lblOutput.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblOutput.Location = new System.Drawing.Point(30, 194);
            this.lblOutput.MinimumSize = new System.Drawing.Size(250, 100);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(250, 100);
            this.lblOutput.TabIndex = 7;
            // 
            // formJalousie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(319, 325);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbxMaterial);
            this.Controls.Add(this.txtbxHeight);
            this.Controls.Add(this.txtbxWidth);
            this.Controls.Add(this.lblMaterial);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblWidth);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.Name = "formJalousie";
            this.Text = "Жалюзи";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtbxWidth;
        private System.Windows.Forms.TextBox txtbxHeight;
        private System.Windows.Forms.ComboBox cmbxMaterial;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblOutput;
    }
}

